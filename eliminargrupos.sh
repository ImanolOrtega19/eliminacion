#!/bin/bash
ROOT_UID=0
SUCCESS=0

if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Ejecute como root este script"
  echo "Format-> sudo ./name_file groups.csv"
  exit $E_NOTROOT
fi  

file=$1

if [ "${file}X" = "X" ];
then
   echo "Debe pasar como parametro groups.csv"
   echo "Format-> sudo ./name_file groups.csv"
   exit 1
fi

eliminarGrupo(){
	eval group="$1"
	groupdel "${group}"
	if [ $? -eq $SUCCESS ];
	then
		echo -e "\e[31m \t\t\t\tGrupo [${group}] eliminado correctamente...\e[0m"
	else
		echo -e "\e[31m \t\t\t\tGrupo [${group}] No se pudo eliminar...\e[0m"
	fi
}
while IFS=: read -r f1
do
	eliminarGrupo "\${f1}"	

done < ${file}

exit 0